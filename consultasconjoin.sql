use universidad;

select * from alumno;
select count(*) as "Número de alumnos" from alumno;

select concat(nombre, " ", apellido1, " ", apellido2) as "Profesor" from profesor;
select nombre from departamento;

-- Esta consulta devuelve del nombre completo del docente con su departamento
select concat(p.nombre, " ", p.apellido1, " ", p.apellido2) as "Profesor",
	d.nombre as "Departamento"
	from profesor as p
	join departamento as d
		on d.id = p.id_departamento;
        

select concat(p.nombre, " ", p.apellido1, " ", p.apellido2) as "Profesor",
	a.nombre as "Asignatura"
	from profesor as p
	join asignatura as a
		on p.id = a.id_profesor;
        

        
select count(*) from asignatura;
        
select concat(p.nombre, " ", p.apellido1, " ", p.apellido2) as "Profesor",
	d.nombre as "Departamento",
    a.nombre as "Asignatura"
	from departamento as d
		join departamento as d
			on p.id_departamento = d.id
				join asignatura as a
					on a.id_profesor = p.id;
    
-- Consulta de tres tablas relacionadas
select a.nombre as "Asignatura",
	p.nombre as "Profesor",
    d.nombre as "Departamento"
	from asignatura as a
		join profesor as p
        on a.id_profesor = p.id
			join departamento as d
			on d.id = p.id_departamento;



-- Consulta de tres tablas relacionadas
select asignatura.nombre as "Asignatura",
	profesor.nombre as "Profesor",
    departamento.nombre as "Departamento"
	from asignatura
		join profesor
        on asignatura.id_profesor = profesor.id
			join departamento
			on departamento.id = profesor.id_departamento;        


-- Consulta de tres tablas relacionadas
select departamento.nombre as "Departamento",
    grado.nombre as "Grado"
    from grado
    join asignatura
	on grado.id = asignatura.id_grado
		join profesor
        on asignatura.id_profesor = profesor.id
			join departamento
			on departamento.id = profesor.id_departamento; 
        
